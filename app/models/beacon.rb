class Beacon
  include Mongoid::Document
  field :lon, type: Float
  field :lat, type: Float
  field :radius, type: Float

  def same_location?(arg)
    self.lon === arg.lon && self.lat === arg.lat
  end
end

