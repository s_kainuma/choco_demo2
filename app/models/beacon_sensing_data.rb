class BeaconSensingData
  include Mongoid::Document
  field :t, type: DateTime
  field :mobile_t, type: DateTime
  field :mobile_uuid, type: String
  field :point_lon, type: Float
  field :point_lat, type: Float
  embeds_many :beacons
end

