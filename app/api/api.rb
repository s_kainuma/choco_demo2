require 'rack/contrib'

class API < Grape::API
  use Rack::JSONP
  format :json

#  http_basic do |email, password|
#    user = User.find_by(email: email)
#    user && user.valid_password?(password)
#  end

  EXPAND = 'expand'

  helpers do
    def mobile_locations_query_core(start_dt, end_dt, mobile_uuids, order_by, limit = 20000)
      term = {:mobile_t.gte => start_dt}
      term.store(:mobile_t.lt, end_dt) unless end_dt.nil?

      beacon_sensing_datas = BeaconSensingData.where(term).in(:mobile_uuid => mobile_uuids).order_by(order_by).limit(limit)
    end

    def conv_heatmap_data(beacon_sensing_data, expand = nil)
      return nil if beacon_sensing_data.point_lon.blank?
      return nil if beacon_sensing_data.point_lat.blank?

      obj = calc_ll beacon_sensing_data.point_lon.to_f, beacon_sensing_data.point_lat.to_f, expand
      obj[:value] = 1
      obj[:mobile_t] = beacon_sensing_data.mobile_t
      obj[:mobile_uuid] = beacon_sensing_data.mobile_uuid

      return obj
    end

    def calc_ll(in_lon, in_lat, expand = nil)
      # =========================================================
      # 上田 本社 
=begin
      h_img_pixs = {:x => 750, :y => 500}
      #h_lon_range = {:min => 138.2274, :max => 138.22775}
      h_lon_range = {:min => 138.22748, :max => 138.2278}
      #h_lat_range = {:min => 36.3634, :max => 36.3636}
      h_lat_range = {:min => 36.36342, :max => 36.36363}

      deg = -14.3
=end

      # 諏訪圏
      #h_img_pixs = {:x => 750, :y => 500}
      h_img_pixs = {:x => 660, :y => 440}
      #h_lon_range = {:min => 138.1081950, :max => 138.1085600}
      h_lon_range = {:min => 138.1082300, :max => 138.1085800}
      #h_lat_range = {:min => 36.04330000, :max => 36.04354000}
      h_lat_range = {:min => 36.04345000, :max => 36.04352500}

      #deg = -23.0
      deg = 0

      # 拡大版
      if (expand == EXPAND) then
        #h_lon_range = {:min => 138.1082900, :max => 138.1083950}
        h_lon_range = {:min => 138.1082990, :max => 138.1083990}
        #h_lat_range = {:min => 36.04331650, :max => 36.04338900}
        h_lat_range = {:min => 36.04335650, :max => 36.04337900}
      end
      # =========================================================

      pos = {}

      # === 緯度→X軸の位置(ピクセル)
      max_lon = h_lon_range[:max]
      min_lon = h_lon_range[:min]
      x_pixs = h_img_pixs[:x]
      # 1ピクセルあたりの緯度
      uni_x = (max_lon - min_lon) / x_pixs
      return nil if uni_x == 0
      # 画像の左端からのピクセル数を計算（小数点以下四捨五入）
      x = ((in_lon - min_lon) / uni_x).round

      # === 経度→Y軸の位置(ピクセル)
      max_lat = h_lat_range[:max]
      min_lat = h_lat_range[:min]
      y_pixs = h_img_pixs[:y]
      # 1ピクセルあたりの経度
      uni_y = (max_lat - min_lat) / y_pixs
      return nil if uni_y == 0
      # 画像の上端からのピクセル数を計算（小数点以下四捨五入）
      y = ((max_lat - in_lat) / uni_y).round

      # === 回転
      #p 'Math'
      rad = deg2rad(deg)
      #p 'x = ' + x.to_s
      #p 'y = ' + y.to_s

      y = y * 0.415
      #x = x * 0.92


      #ｘ’＝ｘcosθ-ysinθ 
      xd = x * Math::cos(rad) - y * Math::sin(rad)
      #ｙ’＝ｘsinθ+ycosθ
      yd = x * Math::sin(rad) + y * Math::cos(rad)

      # === 上下逆転
      yd = h_img_pixs[:y] - yd

      # === 左右逆転
      xd = h_img_pixs[:x] - xd

      #p 'xd = ' + xd.to_s
      #p 'yd = ' + yd.to_s
     
      #pos[:x] = x
      #pos[:y] = y 
      pos[:x] = xd
      pos[:y] = yd 

      return pos
    end

    def deg2rad(deg)
      deg * Math::PI/ 180.0
    end
  end

  resource :mobile_locations do
    desc ""
    get do
      start_dt = params[:start_dt]
      start_dt = start_dt + '.000+0000'
      end_dt = nil

      expand = params[:expand]
      expnad ||= EXPAND

      order_by = []
      order_by.push([:mobile_t, :desc])

      beacon_sensing_datas = mobile_locations_query_core(start_dt, end_dt, params[:mobile_uuids], order_by, 240)

      mobile_locations = []

      beacon_sensing_datas.each{ |beacon_sensing_data|
        val = conv_heatmap_data(beacon_sensing_data, expand)
        mobile_locations << val if val.present?
      }
      mobile_locations
    end

    desc ""
    get :by_hour do
    query = params[:date] + ' ' + params[:hour] + '%'
#    select = "sensor_id, temperture, hermidity, DATE_FORMAT(log_datetime, '%Y-%m-%d %H:%i')"
    select = "sensor_id, temperture, hermidity, log_datetime"
    group_by = "DATE_FORMAT(log_datetime, '%Y-%m-%d %H:%i'), sensor_id"
    sensor_logs = SensorLog.where('log_datetime LIKE ?' ,query)
    sensor_logs = sensor_logs.select(select)
    sensor_logs = sensor_logs.group(group_by)
    temp_avg = sensor_logs.average('temperture')
    herm_avg = sensor_logs.average('hermidity')

    mobile_locations = []

    tmp_heatmaps = []
      sensor_logs.each do |sensor_log|
        #tmp_dataを組み立てる
        tmp_data = {}
        tmp_data[:sensor_id] = sensor_log.sensor_id
        val = sensor_log.temperture.to_f;
        tmp_data[:temperture] = (-39.4 + 0.01 * val)
        val = sensor_log.hermidity.to_f;
        tmp_data[:hermidity] = -2.0468 + 0.0367*val - 1.5955*val*val/1000000
        str = sensor_log.log_datetime.strftime("%Y-%m-%d %H:%M:%S")
        tmp_data[:datetime] = str
        #tmp_heatmapsに溜め込む
        tmp_heatmaps.push(tmp_data)
      end
      tmp_heatmaps_result = {}
      tmp_heatmaps_result[:datas] = tmp_heatmaps
      mobile_locations << tmp_heatmaps_result

    end

    desc ""
    get :by_day do
      date = params[:date]
      start_dt = '07:00:00.000+0000'
      end_dt = '21:00:00.000+0000'

      start_dt = date + ' ' + start_dt
      end_dt = date + ' ' + end_dt

      order_by = []
      order_by.push([:mobile_uuid, :asc])
      order_by.push([:mobile_t, :asc])

      beacon_sensing_datas = mobile_locations_query_core(start_dt, end_dt, params[:mobile_uuids], order_by)

      mobile_locations = []

      mobile_uuid = nil
      prev_mobile_uuid = nil
      tmp_datas = {}
      0.step(60,5) { |i|
        tmp_datas[format("%02d",i)] = [] #Array.new #[] 
      }

      beacon_sensing_datas.each{ |beacon_sensing_data|

        mobile_uuid = beacon_sensing_data.mobile_uuid

        if prev_mobile_uuid.present? && mobile_uuid != prev_mobile_uuid then
          tmp_mobile_data = {}
          tmp_mobile_data[:mobile_uuid] = prev_mobile_uuid
          tmp_mobile_data[:datas] = tmp_datas
          mobile_locations << tmp_mobile_data

          tmp_datas = {}
          0.step(60,5) { |i|
            tmp_datas[format("%02d",i)] = [] #Array.new #[] 
          }
        end

        tmp_mobile_location = conv_heatmap_data(beacon_sensing_data)
        tmp_mobile_t = beacon_sensing_data.mobile_t

        hour = tmp_mobile_t.hour
        idx = (hour - 7) * 5

        tmp_datas[format("%02d",idx)] << tmp_mobile_location if tmp_mobile_location.present?

        prev_mobile_uuid = mobile_uuid
      }

      tmp_mobile_data = {}
      tmp_mobile_data[:mobile_uuid] = prev_mobile_uuid
      tmp_mobile_data[:datas] = tmp_datas
      mobile_locations << tmp_mobile_data
    end
  end

  resource :beacons do
    get do
      #beacons = HTTParty.get('http://54.64.169.207/beacon/api/beacons/')
      #beacons = HTTParty.get('http://localhost:3000/beacon/api/beacons/')
      conv_beacons = []

      expand = params[:expand]
      #expnad ||= nil

      #beacons.each{ |beacon|
      #  conv_beacons << calc_ll(beacon['lon'].to_f, beacon['lat'].to_f, expand)
      #}

=begin
      conv_beacons << calc_ll(138.1083982, 36.04341396) #75
      conv_beacons << calc_ll(138.1084172, 36.04342006) #76
      conv_beacons << calc_ll(138.1084122, 36.04337906) #77
      conv_beacons << calc_ll(138.1083832, 36.04342106) #78
      conv_beacons << calc_ll(138.1083582, 36.04342206) #79
      conv_beacons << calc_ll(138.1083312, 36.04342456) #80
      conv_beacons << calc_ll(138.1083702, 36.04346506) #82
      conv_beacons << calc_ll(138.1083452, 36.04346006) #83
      conv_beacons << calc_ll(138.1083222, 36.04345406) #84
=end
      conv_beacons << {"x":343.0,"y":169.835} #75
      conv_beacons << {"x":307.0,"y":184.36} #76
      conv_beacons << {"x":316.0,"y":84.76} #77
      conv_beacons << {"x":371.0,"y":186.85} #78
      conv_beacons << {"x":418.0,"y":189.34} #79
      conv_beacons << {"x":469.0,"y":195.565} #80
      conv_beacons << {"x":396.0,"y":293.92} #82
      conv_beacons << {"x":443.0,"y":281.885} #83
      conv_beacons << {"x":486.0,"y":267.36} #84

      conv_beacons
    end
  end

  resource :booths do
    get do
      Booth.all
    end
  end

end
