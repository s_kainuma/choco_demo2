json.array!(@booths) do |booth|
  json.extract! booth, :id, :title, :no
  json.url booth_url(booth, format: :json)
end
