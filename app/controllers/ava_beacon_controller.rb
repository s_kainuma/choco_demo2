class AvaBeaconController < ApplicationController
  layout 'beacon'

  EXPAND = 'expand'

  # for login
#  before_filter :authenticate_user!


  def tracking_realtime
    @expand = EXPAND
  end

  def heatmap_realtime
    @expand = EXPAND
  end

  def heatmap_term
    @expand = nil
  end

end
